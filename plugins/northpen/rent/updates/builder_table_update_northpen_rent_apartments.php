<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNorthpenRentApartments extends Migration
{
    public function up()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->double('price', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->dropColumn('price');
        });
    }
}
