<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNorthpenRentApartments2 extends Migration
{
    public function up()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->text('icons')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->dropColumn('icons');
        });
    }
}
