<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNorthpenRentSliders extends Migration
{
    public function up()
    {
        Schema::create('northpen_rent_sliders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('pic', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('northpen_rent_sliders');
    }
}
