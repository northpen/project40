<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNorthpenRentApartments4 extends Migration
{
    public function up()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->boolean('hidden_apartment')->nullable();
            $table->boolean('long_term')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->dropColumn('hidden_apartment');
            $table->dropColumn('long_term');
        });
    }
}
