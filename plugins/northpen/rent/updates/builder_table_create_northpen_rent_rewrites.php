<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNorthpenRentRewrites extends Migration
{
    public function up()
    {
        Schema::create('northpen_rent_rewrites', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('northpen_rent_rewrites');
    }
}
