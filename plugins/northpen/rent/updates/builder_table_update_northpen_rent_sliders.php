<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNorthpenRentSliders extends Migration
{
    public function up()
    {
        Schema::table('northpen_rent_sliders', function($table)
        {
            $table->string('title', 255)->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('pic', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('northpen_rent_sliders', function($table)
        {
            $table->dropColumn('title');
            $table->increments('id')->unsigned()->change();
            $table->string('pic', 255)->nullable(false)->change();
        });
    }
}
