<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNorthpenRentApartments extends Migration
{
    public function up()
    {
        Schema::create('northpen_rent_apartments', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('pic_review', 255)->nullable();
            $table->integer('rooms')->nullable();
            $table->text('pic_sliders')->nullable();
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_to')->nullable();
            $table->string('address', 512)->nullable();
            $table->text('description')->nullable();
            $table->text('little_description')->nullable();
            $table->text('relax')->nullable();
            $table->text('rules')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('northpen_rent_apartments');
    }
}
