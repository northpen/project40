<?php namespace Northpen\Rent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNorthpenRentApartments3 extends Migration
{
    public function up()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->string('desciption_short', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('northpen_rent_apartments', function($table)
        {
            $table->dropColumn('desciption_short');
        });
    }
}
