<?php namespace Northpen\Rent\Models;

use Model;

/**
 * Model
 */
class Sliders extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'northpen_rent_sliders';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
